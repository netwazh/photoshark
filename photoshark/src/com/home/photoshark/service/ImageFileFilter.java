package com.home.photoshark.service;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * File filter to allow only image files to be returned in call to
 * listFiles(FileFilter)
 * 
 */
public class ImageFileFilter implements FileFilter {

	private List<String> fileExtension = new ArrayList<String>();
	private boolean isDirectoryIncluded;

	public ImageFileFilter(List<String> fileExtensions, boolean allowDirectory) {
		this.setFileExtension(fileExtensions);
		this.setDirectoryIncluded(allowDirectory);
	}

	@Override
	public boolean accept(File pathname) {
		if (isDirectoryIncluded && pathname.isDirectory())
			return true;
		if (pathname.getName().toLowerCase().lastIndexOf('.') != -1) {
			String targetFileExtension = pathname.getName().toLowerCase().substring(pathname.getName().toLowerCase().lastIndexOf('.') + 1);
			return fileExtension.contains(targetFileExtension);
		} else {
			return false;
		}
	}

	public List<String> getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(List<String> fileExtension) {
		this.fileExtension = fileExtension;
	}

	public boolean isDirectoryIncluded() {
		return isDirectoryIncluded;
	}

	public void setDirectoryIncluded(boolean isDirectoryIncluded) {
		this.isDirectoryIncluded = isDirectoryIncluded;
	}

}
