package com.home.photoshark.controller;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import com.home.photoshark.model.ApplicationModel;
import com.home.photoshark.model.ResourceBundleModel;
import com.home.photoshark.model.event.DetailsPanelModelEvent;
import com.home.photoshark.view.Application;
import com.home.photoshark.view.InfoPanelView;

public class InfoPanelController implements Observer {

	private Logger logger = Logger.getLogger(InfoPanelController.class.getName());
	private InfoPanelView infoPanelView;

	public InfoPanelController(InfoPanelView v) {
		logger.addHandler(Application.logFileHandler);
		this.infoPanelView = v;
	}

	/**
	 * The controller is an {@link Observer} object. It observe the changes of
	 * models, and update the view it take responsibility of.
	 */
	@Override
	public void update(Observable o, Object arg) {

		if (o instanceof ResourceBundleModel) {
			logger.info("update infopanelview bacause of reseourceBundleModel changed");
			infoPanelView.updateViewForResourceBundleChange();
		} else if (o instanceof ApplicationModel) {

			if (arg == DetailsPanelModelEvent.PHOTO_TABLE_MODEL_CHANGE) {
				// TODO: update total number of loaded image text
				int n = this.infoPanelView.getApplicationFrame().applicationModel.getPhotoTableModel().getImageArray().size();
				infoPanelView.setTotalImageNumberLabelText(n);
			}

		} else {
			logger.warning("unrecognized observable model");
		}

	}

}
