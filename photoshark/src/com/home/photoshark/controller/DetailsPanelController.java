package com.home.photoshark.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.home.photoshark.model.ApplicationModel;
import com.home.photoshark.model.event.DetailsPanelModelEvent;
import com.home.photoshark.view.Application;
import com.home.photoshark.view.DetailsPanelView;
import com.home.photoshark.view.MainPanelView;

public class DetailsPanelController implements Observer, ListSelectionListener {
	private Logger logger = Logger.getLogger(DetailsPanelController.class.getName());
	private DetailsPanelView detailsPanelview;

	public DetailsPanelController(DetailsPanelView view) {
		logger.addHandler(Application.logFileHandler);
		this.detailsPanelview = view;
	}

	/**
	 * The controller is an {@link Observer} object. It observe the changes of
	 * models, and update the view it take responsibility of.
	 */
	@Override
	public void update(Observable o, Object arg) {

		if (o instanceof ApplicationModel) {
			if (arg == DetailsPanelModelEvent.PHOTO_TABLE_MODEL_CHANGE) {
				detailsPanelview.getDetailsPanelTable().setModel(((ApplicationModel) o).getPhotoTableModel());
			}
			if (arg == DetailsPanelModelEvent.SELECTED_FOLDER_CHANGE) {
				logger.info("update details panel view becuase of DetailsPanelModel selectedFolders change");
			}

		} else {
			logger.warning("unrecognized observable model");
		}

	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		handlePreviewFile();
	}

	
	private void handlePreviewFile() {
		if (detailsPanelview.getDetailsPanelTable().getSelectedRowCount() != 1) {
			detailsPanelview.showMessageDialog("Must select ONE file to preview.", "Warning", JOptionPane.WARNING_MESSAGE);
		} else {
			// preview selected image
			File file = new File(detailsPanelview.getPreviewFilePath());
			BufferedImage image = null;
			try {
				image = ImageIO.read(file);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			((MainPanelView)detailsPanelview.getParent()).getPreviewPanel().showPreviewImage(image);
		}

	}
	
}
