package com.home.photoshark.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import com.home.photoshark.model.ResourceBundleModel;
import com.home.photoshark.view.Application;
import com.home.photoshark.view.TopMenuView;

public class TopMenuController implements Observer, ActionListener {

	private Logger logger = Logger.getLogger(TopMenuController.class.getName());
	private TopMenuView view;

	public TopMenuController(TopMenuView view) {
		logger.addHandler(Application.logFileHandler);
		this.view = view;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(TopMenuView.ACTION_COMMAND_MENU_ITEM_CLICKED_CHINESE)) {
			view.getApplicationFrame().resourceBundleModel.updateResourceBundleBasedOnLocale(Locale.CHINA);
		}

		if (e.getActionCommand().equals(TopMenuView.ACTION_COMMAND_MENU_ITEM_CLICKED_ENGLISH)) {
			view.getApplicationFrame().resourceBundleModel.updateResourceBundleBasedOnLocale(Locale.ENGLISH);
		}
	}

	/**
	 * The controller is an {@link Observer} object. It observe the changes of
	 * models, and update the view it take responsibility of.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof ResourceBundleModel) {
			logger.info("update TopMenuView bacause of reseourceBundleModel has changed");
			view.updateViewForResourceBundleChange();
		} else {
			logger.warning("unrecognized observable model");
		}

	}

}
