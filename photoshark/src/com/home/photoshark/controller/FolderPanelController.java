package com.home.photoshark.controller;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import com.home.photoshark.model.ApplicationModel;
import com.home.photoshark.model.event.DetailsPanelModelEvent;
import com.home.photoshark.view.Application;
import com.home.photoshark.view.FolderPanelView;

public class FolderPanelController implements Observer {

	private Logger logger = Logger.getLogger(FolderPanelController.class.getName());
	private FolderPanelView folderPanelView;

	public FolderPanelController(FolderPanelView folderPanelView) {
		logger.addHandler(Application.logFileHandler);
		this.folderPanelView = folderPanelView;

	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof ApplicationModel) {
			if (arg == DetailsPanelModelEvent.SELECTED_FOLDER_CHANGE) {
				logger.info(" folder panel controller get selected folder change action");
				folderPanelView.updateLoadedFoldersView(((ApplicationModel) o).getSelectedFolders());
			}
		} else {
			logger.warning("unrecognized observable model");
		}

	}

}
