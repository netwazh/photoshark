package com.home.photoshark.controller;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import com.home.photoshark.model.ResourceBundleModel;
import com.home.photoshark.service.ImageFileFilter;
import com.home.photoshark.service.ImageService;
import com.home.photoshark.util.FileExtension;
import com.home.photoshark.view.Application;
import com.home.photoshark.view.ToolsPanelView;

public class ToolsPanelController implements Observer, ActionListener {

	private Logger logger = Logger.getLogger(ToolsPanelController.class.getName());
	private ToolsPanelView toolsPanelView;

	private ImageService imageService;
	private ImageFileFilter imageFileFilter;

	public ToolsPanelController(ToolsPanelView view) {
		logger.addHandler(Application.logFileHandler);
		this.toolsPanelView = view;
		this.imageService = new ImageService();
		List<String> allowedExtensions = new ArrayList<String>();
		allowedExtensions.add(FileExtension.JPG.name().toLowerCase());
		allowedExtensions.add(FileExtension.PNG.name().toLowerCase());
		this.imageFileFilter = new ImageFileFilter(allowedExtensions, true);
	}

	public ToolsPanelView getToolsPanelView() {
		return toolsPanelView;
	}

	/**
	 * The controller listener to user-actions performed on the view and handle
	 * {@link ActionEvent}.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equals(ToolsPanelView.ACTION_COMMAND_SELECT_DIRECTORY_BUTTON)) {
			handleSelectDirectory();
		} else if (e.getActionCommand().equals(ToolsPanelView.ACTION_COMMAND_RELOAD_DIRECTORY_BUTTON)) {
			handleReloadDirectory();
		} else if (e.getActionCommand().equals(ToolsPanelView.ACTION_COMMAND_RENAME_FILE_BUTTON)) {
			handleRenameFile();
		} else if (e.getActionCommand().equals(ToolsPanelView.ACTION_COMMAND_DELETE_FILE_BUTTON)) {
			handleDeleteFile();
		} else if (e.getActionCommand().equals(ToolsPanelView.ACTION_COMMAND_DELETE_ALL_DUPLICATED_BUTTON)) {
			handleDeleteAllDuplicatedFile();
		} else if (e.getActionCommand().equals(ToolsPanelView.ACTION_COMMAND_OPEN_SELECTED_FOLDER_BUTTON)) {
			handleOpenSelectedFolder();
		} else if (e.getActionCommand().equals(ToolsPanelView.ACTION_COMMAND_ORGANIZE_BUTTON)) {
			handleOrganize();
		}

		// handle other action
	}

	private void handleOrganize() {
		if (this.toolsPanelView.getApplicationFrame().applicationModel.getSelectedFolders().size() != 0) {
			File sourceDir = null;
			File directorySelected = null;

			sourceDir = this.toolsPanelView.getApplicationFrame().applicationModel.getSelectedFolders().get(0);
			directorySelected = getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().getHandleOrganizeFolder();

			if (directorySelected == null) {
				throw new RuntimeException("No file selected!");
			} else {
				// TODO: provide options to user to select
				organizeFile(sourceDir, directorySelected, 3);
			}

		} else {
			getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().showMessageDialog("No selected folder", "Warning", JOptionPane.WARNING_MESSAGE);
		}

	}

	private void organizeFile(File sourceDir, File targetDir, int option) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		// First traverse the sourceDir
		if (sourceDir.isDirectory()) {
			File[] childNodes = sourceDir.listFiles();
			for (File childNode : childNodes) {
				organizeFile(childNode, targetDir, option);
			}
		} else {
			// get the file's date information
			// get the file's date with format yy-mm-dd
			String dateString = dateFormat.format(new Date(sourceDir.lastModified()));
			// get the file's last modified year
			String year = dateString.substring(0, 4);
			// get the file's last modified month
			String month = dateString.substring(5, 7);
			// get the file's last modified day
			String day = dateString.substring(8, 10);

			// print the file's information on screen
			logger.info("sourceDir: " + sourceDir.getName() + "Last modified: " + dateString);
			// get the target directories' path information
			// get the path of the 1st class directory that saves the managed
			// files
			String parentPath = targetDir.getPath();
			// initialise to save the lower class directory's path information
			String path = "";

			// get the lower class directory's path information
			switch (option) {
			case 1: // file sorted based on year(year)

				// 2nd class directory's path
				path = parentPath + File.separator + year;
				break;

			case 2: // file sorted based on month(year-month)

				// 3rd class directory's path
				path = parentPath + File.separator + year + File.separator + year + "-" + month;
				break;

			case 3: // file sorted based on day(year-month-day)

				// 4th class directory's path
				path = parentPath + File.separator + year + File.separator + year + "-" + month + File.separator + year + "-" + month + "-" + day;
				break;
			}

			// create the directories of all classes
			File directory = new File(path);
			if (!directory.exists()) {
				directory.mkdirs();
			}

			// copy the file to the target directory
			Path source = sourceDir.toPath();
			Path target = directory.toPath().resolve(sourceDir.getName());
			try {
				Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void handleOpenSelectedFolder() {

		if (this.toolsPanelView.getApplicationFrame().applicationModel.getSelectedFolders().size() != 0) {

			Desktop desktop;
			if (Desktop.isDesktopSupported()) {
				desktop = Desktop.getDesktop();
				try {
					if (desktop.isSupported(Desktop.Action.OPEN)) {
						desktop.open(this.toolsPanelView.getApplicationFrame().applicationModel.getSelectedFolders().get(0));
					}
				} catch (IOException e) {
					getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().showMessageDialog("failed to open folder", "Error", JOptionPane.ERROR_MESSAGE);
					;
				}
			}
		} else {
			getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().showMessageDialog("No selected folder", "Warning", JOptionPane.WARNING_MESSAGE);
		}

	}

	private void handleDeleteAllDuplicatedFile() {
		if (getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().confirmDeleteAll("Do you want to delete all following duplicated image files?(marked with yellow color)")) {
			List<String> results = imageService.removeDuplicated();
			if (results.size() > 0) {
				System.err.println(results);
				StringBuilder errorMessage = new StringBuilder("The following file deletion has been failed:\n");
				for (String fileNameString : results) {
					errorMessage.append(fileNameString + "\n");
				}
				getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().showMessageDialog(errorMessage.toString(), "file to delete", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void handleDeleteFile() {
		if (getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().getDetailsPanelTable().getSelectedRowCount() != 1) {
			getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().showMessageDialog("Must select ONE file to delete.", "Info", JOptionPane.INFORMATION_MESSAGE);
		} else {
			if (imageService.deleteFile(getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().getDetailsPanelTable()
					.convertRowIndexToModel(getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().getDetailsPanelTable().getSelectedRow())) == false) {
				getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().showMessageDialog("failed to delete file", "Info", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	private void handleRenameFile() {
		if (getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().getDetailsPanelTable().getSelectedRowCount() != 1) {
			getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().showMessageDialog("Must select ONE file to rename.", "Info", JOptionPane.INFORMATION_MESSAGE);
		} else {
			// FIXME: input file name should NOT with extension, because the
			// user should not be able to change file type
			String newFileName = (String) JOptionPane.showInputDialog(getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel(), "input new file name with extension:", "Rename file",
					JOptionPane.PLAIN_MESSAGE);
			if (newFileName != null) {
				if (imageService.renameFile(
						getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().getDetailsPanelTable()
								.convertRowIndexToModel(getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().getDetailsPanelTable().getSelectedRow()), newFileName) == false) {
					getToolsPanelView().getApplicationFrame().getMainPanel().getDetailsPanel().showMessageDialog("failed to rename file", "Info", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
	}

	private void handleReloadDirectory() {
		if (this.toolsPanelView.getApplicationFrame().applicationModel.getSelectedFolders().isEmpty()) {
			this.toolsPanelView.getApplicationFrame().getMainPanel().getDetailsPanel().showMessageDialog("No selected folder yet!!", "Info", JOptionPane.INFORMATION_MESSAGE);
		} else {
			File directorySelected = this.toolsPanelView.getApplicationFrame().applicationModel.getSelectedFolders().get(0);
			this.toolsPanelView.getApplicationFrame().applicationModel.setPhotoTable(imageService.load(directorySelected, imageFileFilter));
		}
	}

	private void handleSelectDirectory() {
		File directorySelected = this.toolsPanelView.selectDirectory();
		if (directorySelected == null || !directorySelected.isDirectory()) {
			this.toolsPanelView.getApplicationFrame().getMainPanel().getDetailsPanel().showMessageDialog("No directory selected!", "Info", JOptionPane.INFORMATION_MESSAGE);
		} else {
			logger.info("New selected folder added: " + directorySelected.getAbsolutePath());
			this.toolsPanelView.getApplicationFrame().applicationModel.addSelectedFolder(directorySelected);
			this.toolsPanelView.getApplicationFrame().applicationModel.setPhotoTable(imageService.load(directorySelected, imageFileFilter));
		}
	}

	/**
	 * The controller is an {@link Observer} object. It observe the changes of
	 * models, and update the view it take responsibility of.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof ResourceBundleModel) {
			logger.info("update ToolsMenuView bacause of reseourceBundleModel changed");
			toolsPanelView.updateViewForResourceBundleChange();
		} else {
			logger.warning("unrecognized observable model");
		}
	}

}
