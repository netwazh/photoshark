package com.home.photoshark.model;

import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

/**
 * {@link ResourceBundleModel} hold data for I18N resources. This model is an
 * {@link Observable} object. Controllers of the application are
 * {@link Observer}, which observe changes of this model.
 * 
 */
public class ResourceBundleModel extends Observable {

	private ResourceBundle resourceBundle;

	public ResourceBundleModel() {
		// Initialise default application i18n bundle
		resourceBundle = ResourceBundle.getBundle("i18n", Locale.ENGLISH);
	}

	/**
	 * Update this model's {@link ResourceBundle} with provided {@link Locale}
	 * 
	 * @param locale
	 */
	public void updateResourceBundleBasedOnLocale(Locale locale) {
		resourceBundle = ResourceBundle.getBundle("i18n", locale);
		ResourceBundle.clearCache();
		setChanged();
		notifyObservers();
	}

	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

}
