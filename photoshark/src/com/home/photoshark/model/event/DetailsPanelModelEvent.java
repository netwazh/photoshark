package com.home.photoshark.model.event;

public enum DetailsPanelModelEvent {

	SELECTED_FOLDER_CHANGE, PHOTO_TABLE_MODEL_CHANGE;

}
