package com.home.photoshark.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import com.home.photoshark.model.event.DetailsPanelModelEvent;

/**
 * {@link ApplicationModel} holds application-level data for application. it is
 * an {@link Observable} object, which will be observed by {@link Observer}
 * controllers.
 */
public class ApplicationModel extends Observable {

	private PhotoTableModel photoTableModel;
	private List<File> selectedFolders;

	public ApplicationModel() {
		this.selectedFolders = new ArrayList<File>();
		this.photoTableModel = new PhotoTableModel();
	}

	public List<File> getSelectedFolders() {
		return selectedFolders;
	}

	public void addSelectedFolder(File dir) {
		selectedFolders.add(dir);
		setChanged();
		notifyObservers(DetailsPanelModelEvent.SELECTED_FOLDER_CHANGE);
	}

	public void setPhotoTable(PhotoTableModel p) {
		photoTableModel = p;
		setChanged();
		notifyObservers(DetailsPanelModelEvent.PHOTO_TABLE_MODEL_CHANGE);
	}

	public PhotoTableModel getPhotoTableModel() {
		return photoTableModel;
	}

}
