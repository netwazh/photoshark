package com.home.photoshark.util;

public class ColumnNames {
	public static final String FILE_PROPERTY_COLUMN_NAME = "Name";
	public static final String FILE_PROPERTY_COLUMN_PATH = "Path";
	public static final String FILE_PROPERTY_COLUMN_SIZE = "Size";
	public static final String FILE_PROPERTY_COLUMN_CREATION_TIME = "Creation Time";
	public static final String FILE_PROPERTY_COLUMN_MODIFIED_TIME = "Modified Time";

}
