package com.home.photoshark.util;

public enum FileExtension
{
	JPG, PNG, JPEG;
}
