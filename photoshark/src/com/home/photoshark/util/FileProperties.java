package com.home.photoshark.util;

public class FileProperties {
	public static final String FILE_PROPERTY_VARIABLE_NAME = "name";
	public static final String FILE_PROPERTY_VARIABLE_PATH = "path";
	public static final String FILE_PROPERTY_VARIABLE_SIZE = "size";
	public static final String FILE_PROPERTY_VARIABLE_CREATION_TIME = "creationTime";
	public static final String FILE_PROPERTY_VARIABLE_MODIFIED_TIME = "modifiedTime";
	


}
