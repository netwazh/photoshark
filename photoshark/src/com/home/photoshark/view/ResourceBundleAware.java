package com.home.photoshark.view;

import java.util.ResourceBundle;

/**
 * A implementation of this {@link ResourceBundleAware} interface have been
 * using {@link ResourceBundle} in it UI.
 */
public interface ResourceBundleAware {

	/**
	 * When the {@link ResourceBundle} has been updated, this method is called
	 * to update the {@link ResourceBundleAware} view to reflect the
	 * {@link ResourceBundle} change.
	 */
	void updateViewForResourceBundleChange();

}
