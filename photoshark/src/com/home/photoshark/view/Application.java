package com.home.photoshark.view;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import com.home.photoshark.util.SpalashScreen;

/**
 * The entry point of this application.
 */
public class Application {
	public static FileHandler logFileHandler;

	public static void main(String[] args) {
		Logger logger = null;
		try {
			logFileHandler = new FileHandler("log/application.log");
			logFileHandler.setFormatter(new SimpleFormatter());

			logger = Logger.getLogger(Application.class.getName());

			try {
				for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
					if ("Nimbus".equals(info.getName())) {
						UIManager.setLookAndFeel(info.getClassName());
						break; 
					}
				}
			} catch (Exception e) {
				// use system look and feel if Nimbus is not available
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			}

			final SpalashScreen splashScreen = new SpalashScreen();
			splashScreen.showSplash(new Runnable() {
				public void run() {
					ApplicationFrame windowFrame = new ApplicationFrame("PhotoShark", 1000, 600);
					splashScreen.setVisible(false);
					windowFrame.setVisible(true);
				}
			});
		} catch (Exception e) {
			if (logger != null) {
				logger.log(Level.SEVERE, "Failed to initialize application!");
			}
			System.exit(0);
		}

	}

}
