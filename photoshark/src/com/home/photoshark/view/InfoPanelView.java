package com.home.photoshark.view;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import com.home.photoshark.controller.InfoPanelController;

public class InfoPanelView extends JPanel implements ResourceBundleAware {
	private static final long serialVersionUID = 1L;

	private static final String KEY_TOTAL_IMAGE_LOADED_TEXT = "label.infopanel.totalimageloaded.text";

	// view components
	private JPanel totalImageInfoPanel;
	private JLabel totalImageNumberInFolderLabel;

	private ApplicationFrame applicationFrame;
	private InfoPanelController infoPanelController;
	

	public InfoPanelView(ApplicationFrame applicationFrame) {
		this.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		((FlowLayout) this.getLayout()).setAlignment(FlowLayout.LEFT);

		this.applicationFrame = applicationFrame;
		this.infoPanelController = new InfoPanelController(this);

		totalImageInfoPanel = new JPanel();
		totalImageInfoPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		this.add(totalImageInfoPanel);

		totalImageNumberInFolderLabel = new JLabel(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_TOTAL_IMAGE_LOADED_TEXT));
		totalImageInfoPanel.add(totalImageNumberInFolderLabel);

	}

	public InfoPanelController getInfoPanelController() {
		return infoPanelController;
	}

	@Override
	public void updateViewForResourceBundleChange() {
		totalImageNumberInFolderLabel.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_TOTAL_IMAGE_LOADED_TEXT));
	}

	public void setTotalImageNumberLabelText(int number) {
		totalImageNumberInFolderLabel.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_TOTAL_IMAGE_LOADED_TEXT) + " : " + number);
	}

	public ApplicationFrame getApplicationFrame(){
		return this.applicationFrame;
	}
	
}
