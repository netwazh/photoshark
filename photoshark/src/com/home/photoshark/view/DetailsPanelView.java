package com.home.photoshark.view;

import java.awt.Color;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import com.home.photoshark.controller.DetailsPanelController;
import com.home.photoshark.model.PhotoTableModel;

public class DetailsPanelView extends JScrollPane {
	private static final long serialVersionUID = 1L;
	
	// view components
	private DetailsPanelTable detailsPanelTable;

	private DetailsPanelController detailsPanelController;
	
	public DetailsPanelView() {
		this.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
		this.detailsPanelController = new DetailsPanelController(this);

		this.detailsPanelTable = new DetailsPanelTable();
		this.detailsPanelTable.setFillsViewportHeight(true);
		this.detailsPanelTable.getSelectionModel().addListSelectionListener(detailsPanelController);
		this.setViewportView(detailsPanelTable);
	}

	public DetailsPanelTable getDetailsPanelTable() {
		return detailsPanelTable;
	}

	public DetailsPanelController getDetailsPanelController() {
		return detailsPanelController;
	}

	public File getHandleOrganizeFolder() {

		JOptionPane.showMessageDialog(this, "Select folder to save your managed files", "Info", JOptionPane.INFORMATION_MESSAGE);
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnValue = fileChooser.showOpenDialog(this);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			return fileChooser.getSelectedFile();
		} else {
			return null;
		}

	}

	public boolean confirmDeleteAll(String message) {
		if(JOptionPane.showConfirmDialog(this, message, "Delete all duplicated?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
			return true;
		}
		return false;
	}

	public void showMessageDialog(String message, String title, int messageType) {
		JOptionPane.showMessageDialog(this, message, title, messageType);
	}

	

	/**
	 * Get the selected file in the main panel image table, for preview this image.
	 * @return
	 */
	public String getPreviewFilePath() {
		PhotoTableModel photoTableModel = (PhotoTableModel) this.getDetailsPanelTable().getModel();
		int index = this.getDetailsPanelTable().getSelectedRow();
		return photoTableModel.getImageArray().get(index).getPath();
	}
	  
}
