package com.home.photoshark.view;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;

import com.home.photoshark.controller.FolderPanelController;

public class FolderPanelView extends JScrollPane {

	private static final long serialVersionUID = 1L;

	private FolderPanelController folderPanelController;

	private JPanel contentPanel;

	public FolderPanelView() {

		this.folderPanelController = new FolderPanelController(this);

		// set threshold for JScroll pane
		setPreferredSize(new Dimension(260, 0));
		setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));

		contentPanel = new JPanel();
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setLayout(new MigLayout("fill"));
		JLabel noFolderLoadedText = new JLabel("Select a folder to load images.");
		noFolderLoadedText.setForeground(Color.GRAY);
		contentPanel.add(noFolderLoadedText, "align center");

		setViewportView(contentPanel);
	}

	public FolderPanelController getFolderPanelController() {
		return folderPanelController;
	}

	public void updateLoadedFoldersView(List<File> selectedFolders) {

		contentPanel.removeAll();
		contentPanel.setLayout(new MigLayout());
		for (File f : selectedFolders) {
			contentPanel.add(new JLabel(new ImageIcon("resource/image/added_folder_16x16.png")));
			contentPanel.add(new JLabel(f.getAbsolutePath()), "wrap");
		}

		contentPanel.revalidate();
		contentPanel.repaint();

	}

}
