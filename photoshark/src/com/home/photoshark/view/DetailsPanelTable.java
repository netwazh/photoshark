package com.home.photoshark.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.home.photoshark.model.PhotoTableModel;

public class DetailsPanelTable extends JTable {

	private static final long serialVersionUID = 1L;
	private final TableCellRenderer cellRender = new ColorTableCellRenderer();

	@Override
	public TableCellRenderer getCellRenderer(int row, int column) {

		PhotoTableModel model = (PhotoTableModel) this.getModel();
		if (model.isRowFromDuplicatedImage(row)) {
			return cellRender;
		} else {
			return super.getCellRenderer(row, column);
		}
	}

	private class ColorTableCellRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = 1L;

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			c.setBackground(Color.YELLOW);
			return c;
		}
	}

}
