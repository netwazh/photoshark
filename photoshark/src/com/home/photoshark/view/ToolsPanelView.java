package com.home.photoshark.view;

import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import net.miginfocom.swing.MigLayout;

import com.home.photoshark.controller.ToolsPanelController;

public class ToolsPanelView extends JPanel implements ResourceBundleAware {

	private static final long serialVersionUID = 1L;

	// resource bundle properties
	private static final String KEY_SELECT_DIRECTORY_BUTTON_TEXT = "label.toolspanel.selectdirectorybutton.text";
	private static final String KEY_RELOAD_DIRECTORY_BUTTON_TEXT = "label.toolspanel.reloaddirectorybutton.text";
	private static final String KEY_RENAME_FILE_BUTTON_TEXT = "label.toolspanel.renamefilebutton.text";
	private static final String KEY_DELETE_FILE_BUTTON_TEXT = "label.toolspanel.deletefilebutton.text";
	private static final String KEY_DELETE_ALL_DUPLICATED_BUTTON_TEXT = "label.toolspanel.deleteallduplicatedbutton.text";
	private static final String KEY_OPEN_SELECTED_FOLDER_BUTTON_TEXT = "label.toolspanel.openselectedfolderbutton.text";
	private static final String KEY_ORGANIZE_BUTTON_TEXT = "label.toolspanel.organizebutton.text";

	// action commands
	public static final String ACTION_COMMAND_SELECT_DIRECTORY_BUTTON = "selectDirectoryButtonClicked";
	public static final String ACTION_COMMAND_RELOAD_DIRECTORY_BUTTON = "reloadDirectoryButtonClicked";
	public static final String ACTION_COMMAND_RENAME_FILE_BUTTON = "renameFileButtonClicked";
	public static final String ACTION_COMMAND_DELETE_FILE_BUTTON = "deleteFileButtonClicked";
	public static final String ACTION_COMMAND_DELETE_ALL_DUPLICATED_BUTTON = "deleteAllDuplicatedButtonClicked";
	public static final String ACTION_COMMAND_OPEN_SELECTED_FOLDER_BUTTON = "openSelectedFolderButtonClicked";
	public static final String ACTION_COMMAND_ORGANIZE_BUTTON = "organizeButtonClicked";

	// view components
	private JButton selectDirectoryButton;
	private JButton btnReload;
	private JButton btnRename;
	private JButton btnDelete;
	private JButton btnDeleteAll;
	private JButton btnOpenSelectedFolder;
	private JButton btnOrganize;

	private ApplicationFrame applicationFrame;
	private ToolsPanelController toolsPanelController;

	/**
	 * @param applicationFrame
	 *            The parent frame this view is attached to.
	 */
	public ToolsPanelView(ApplicationFrame applicationFrame) {
		this.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		this.setLayout(new MigLayout("align left"));

		this.applicationFrame = applicationFrame;
		this.toolsPanelController = new ToolsPanelController(this);

		// register action listener for components, the action listener
		// distinguish component with the components' action command
		selectDirectoryButton = new JButton(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_SELECT_DIRECTORY_BUTTON_TEXT));
		selectDirectoryButton.setIcon(new ImageIcon("resource/image/open_folder_64x64.png"));
		selectDirectoryButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		selectDirectoryButton.setHorizontalTextPosition(SwingConstants.CENTER);
		selectDirectoryButton.setActionCommand(ACTION_COMMAND_SELECT_DIRECTORY_BUTTON);
		selectDirectoryButton.addActionListener(toolsPanelController);
		this.add(selectDirectoryButton);

		btnOpenSelectedFolder = new JButton(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_OPEN_SELECTED_FOLDER_BUTTON_TEXT));
		btnOpenSelectedFolder.setIcon(new ImageIcon("resource/image/goto_folder_64x64.png"));
		btnOpenSelectedFolder.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnOpenSelectedFolder.setHorizontalTextPosition(SwingConstants.CENTER);
		btnOpenSelectedFolder.setActionCommand(ACTION_COMMAND_OPEN_SELECTED_FOLDER_BUTTON);
		btnOpenSelectedFolder.addActionListener(toolsPanelController);
		this.add(btnOpenSelectedFolder);
		
		btnReload = new JButton(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_RELOAD_DIRECTORY_BUTTON_TEXT));
		btnReload.setIcon(new ImageIcon("resource/image/reload_64x64.png"));
		btnReload.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnReload.setHorizontalTextPosition(SwingConstants.CENTER);
		btnReload.setActionCommand(ACTION_COMMAND_RELOAD_DIRECTORY_BUTTON);
		btnReload.addActionListener(toolsPanelController);
		this.add(btnReload);

		btnRename = new JButton(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_RENAME_FILE_BUTTON_TEXT));
		btnRename.setIcon(new ImageIcon("resource/image/rename_64x64.png"));
		btnRename.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnRename.setHorizontalTextPosition(SwingConstants.CENTER);
		btnRename.setActionCommand(ACTION_COMMAND_RENAME_FILE_BUTTON);
		btnRename.addActionListener(toolsPanelController);
		this.add(btnRename);

		btnDelete = new JButton(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_DELETE_FILE_BUTTON_TEXT));
		btnDelete.setIcon(new ImageIcon("resource/image/remove_64x64.png"));
		btnDelete.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnDelete.setHorizontalTextPosition(SwingConstants.CENTER);
		btnDelete.setActionCommand(ACTION_COMMAND_DELETE_FILE_BUTTON);
		btnDelete.addActionListener(toolsPanelController);
		this.add(btnDelete);

		btnDeleteAll = new JButton(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_DELETE_ALL_DUPLICATED_BUTTON_TEXT));
		btnDeleteAll.setIcon(new ImageIcon("resource/image/delete_all_64x64.png"));
		btnDeleteAll.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnDeleteAll.setHorizontalTextPosition(SwingConstants.CENTER);
		btnDeleteAll.setActionCommand(ACTION_COMMAND_DELETE_ALL_DUPLICATED_BUTTON);
		btnDeleteAll.addActionListener(toolsPanelController);
		this.add(btnDeleteAll);

		btnOrganize = new JButton(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_ORGANIZE_BUTTON_TEXT));
		btnOrganize.setIcon(new ImageIcon("resource/image/folder_organize_64x64.png"));
		btnOrganize.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnOrganize.setHorizontalTextPosition(SwingConstants.CENTER);
		btnOrganize.setActionCommand(ACTION_COMMAND_ORGANIZE_BUTTON);
		btnOrganize.addActionListener(toolsPanelController);
		this.add(btnOrganize);

	}

	@Override
	public void updateViewForResourceBundleChange() {
		selectDirectoryButton.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_SELECT_DIRECTORY_BUTTON_TEXT));
		btnReload.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_RELOAD_DIRECTORY_BUTTON_TEXT));
		btnRename.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_RENAME_FILE_BUTTON_TEXT));
		btnDelete.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_DELETE_FILE_BUTTON_TEXT));
		btnDeleteAll.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_DELETE_ALL_DUPLICATED_BUTTON_TEXT));
		btnOrganize.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_ORGANIZE_BUTTON_TEXT));
		btnOpenSelectedFolder.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_OPEN_SELECTED_FOLDER_BUTTON_TEXT));

	}

	public ToolsPanelController getToolsPanelController() {
		return toolsPanelController;
	}

	public ApplicationFrame getApplicationFrame() {
		return this.applicationFrame;
	}

	/**
	 * Open a file chooser and let the user select one directory.
	 * 
	 * @return null if no directory is selected
	 */
	public File selectDirectory() {

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnValue = fileChooser.showOpenDialog(this);

		if (returnValue == JFileChooser.APPROVE_OPTION) {
			return fileChooser.getSelectedFile();
		} else {
			return null;
		}

	}
}
