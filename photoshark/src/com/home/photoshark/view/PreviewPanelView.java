package com.home.photoshark.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

public class PreviewPanelView extends JPanel {

	private static final long serialVersionUID = 1L;

	private JLabel notPreviewingText;

	public PreviewPanelView() {
		setLayout(new MigLayout("fill"));
		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(300, 300));
		notPreviewingText = new JLabel("Select a file to preview.");
		notPreviewingText.setForeground(Color.GRAY);
		add(notPreviewingText, "align center");
	}

	/**
	 * Update the preview of given image in preview-view area.
	 */
	public void showPreviewImage(BufferedImage image) {
		removeAll();
		add(new JLabel(new ImageIcon(image.getScaledInstance(getWidth(), -1, Image.SCALE_DEFAULT))));
		revalidate();
		repaint();
	}

}
