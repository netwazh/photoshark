package com.home.photoshark.view;

import java.awt.BorderLayout;

import javax.swing.JPanel;

public class MainPanelView extends JPanel {

	private static final long serialVersionUID = 1L;

	private FolderPanelView folderPanel;
	private DetailsPanelView detailsPanel;
	private PreviewPanelView previewPanel;

	private ApplicationFrame applicationFrame;

	public MainPanelView(ApplicationFrame applicationFrame) {
		this.applicationFrame = applicationFrame;
		this.setLayout(new BorderLayout());

		// init main panel view
		this.folderPanel = new FolderPanelView();
		this.detailsPanel = new DetailsPanelView();
		this.previewPanel = new PreviewPanelView();

		// add sub-views
		add(folderPanel, BorderLayout.WEST);
		add(detailsPanel, BorderLayout.CENTER);
		add(previewPanel, BorderLayout.EAST);
	}

	public FolderPanelView getFolderPanel(){
		return this.folderPanel;
	}

	public DetailsPanelView getDetailsPanel() {
		return this.detailsPanel;
	}
	public PreviewPanelView getPreviewPanel(){
		return this.previewPanel;
	}

	public ApplicationFrame getApplicationFrame() {
		return applicationFrame;
	}
	

}
