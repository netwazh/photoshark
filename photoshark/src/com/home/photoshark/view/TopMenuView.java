package com.home.photoshark.view;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.home.photoshark.controller.TopMenuController;

public class TopMenuView extends JMenuBar implements ResourceBundleAware {

	private static final long serialVersionUID = 1L;

	//resource bundle properties
	private static final String KEY_PREFERENCES_MENU_TEXT = "label.topmenu.preferencemenu.text";
	private static final String KEY_LANGUAGE_MENU_TEXT = "label.topmenu.languagemenu.text";
	private static final String KEY_LANGUAGE_MENU_ITEM_ENGLISH_TEXT = "label.topmenu.languagemenuitem.english.text";
	private static final String KEY_LANGUAGE_MENU_ITEM_CHINESE_TEXT = "label.topmenu.languagemenuitem.chinese.text";
	private static final String KEY_GLOBAL_PREFERENCE_MENU_TEXT = "label.topmenu.globalpreferencemenu.text";
	private static final String KEY_ABOUT_MENU_TEXT = "label.topmenu.aboutmenu.text";
	private static final String KEY_ABOUT_MENU_ITEM_HELP_TEXT = "label.topmenu.aboutmenuitem.help.text";
	private static final String KEY_ABOUT_MENU_ITEM_ABOUT_TEXT = "label.topmenu.aboutmenuitem.about.text";

	//action commands
	public static final String ACTION_COMMAND_MENU_ITEM_CLICKED_ENGLISH = "menuItemEnglishClicked";
	public static final String ACTION_COMMAND_MENU_ITEM_CLICKED_CHINESE = "menuItemChineseClicked";
	
	
	// view components
	private JMenu preferencesMenu;
	private JMenu languageMenu;
	private JMenuItem menuItemEnglish;
	private JMenuItem menuItemChinese;
	private JMenuItem mntmGlobalPreferences;
	private JMenu aboutMenu;
	private JMenuItem menuItemHelp;
	private JMenuItem menuItemAbout;
	
	
	private ApplicationFrame applicationFrame;
	private TopMenuController topMenuController;
	
	public TopMenuView(ApplicationFrame applicationFrame) {
		this.applicationFrame = applicationFrame;
		this.topMenuController = new TopMenuController(this);

		// register action listener for components, the action listener distinguish component with the components' action command
		preferencesMenu = new JMenu(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_PREFERENCES_MENU_TEXT));
		this.add(preferencesMenu);

		languageMenu = new JMenu(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_LANGUAGE_MENU_TEXT));
		menuItemEnglish = new JMenuItem(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_LANGUAGE_MENU_ITEM_ENGLISH_TEXT));
		menuItemEnglish.setActionCommand(ACTION_COMMAND_MENU_ITEM_CLICKED_ENGLISH);
		menuItemEnglish.addActionListener(topMenuController);
		languageMenu.add(menuItemEnglish);

		menuItemChinese = new JMenuItem(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_LANGUAGE_MENU_ITEM_CHINESE_TEXT));
		menuItemChinese.setActionCommand(ACTION_COMMAND_MENU_ITEM_CLICKED_CHINESE);
		menuItemChinese.addActionListener(topMenuController);
		languageMenu.add(menuItemChinese);
		preferencesMenu.add(languageMenu);

		//TODO: not implemented
		mntmGlobalPreferences = new JMenuItem(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_GLOBAL_PREFERENCE_MENU_TEXT));
		preferencesMenu.add(mntmGlobalPreferences);

		//TODO: not implemented
		aboutMenu = new JMenu(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_ABOUT_MENU_TEXT));
		this.add(aboutMenu);

		//TODO: not implemented
		menuItemHelp = new JMenuItem(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_ABOUT_MENU_ITEM_HELP_TEXT));
		aboutMenu.add(menuItemHelp);

		//TODO: not implemented
		menuItemAbout = new JMenuItem(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_ABOUT_MENU_ITEM_ABOUT_TEXT));
		aboutMenu.add(menuItemAbout);

	}

	@Override
	public void updateViewForResourceBundleChange() {
		// use text from new resource bundle
		preferencesMenu.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_PREFERENCES_MENU_TEXT));
		languageMenu.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_LANGUAGE_MENU_TEXT));
		menuItemEnglish.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_LANGUAGE_MENU_ITEM_ENGLISH_TEXT));
		menuItemChinese.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_LANGUAGE_MENU_ITEM_CHINESE_TEXT));
		mntmGlobalPreferences.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_GLOBAL_PREFERENCE_MENU_TEXT));
		aboutMenu.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_ABOUT_MENU_TEXT));
		menuItemHelp.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_ABOUT_MENU_ITEM_HELP_TEXT));
		menuItemAbout.setText(getApplicationFrame().resourceBundleModel.getResourceBundle().getString(KEY_ABOUT_MENU_ITEM_ABOUT_TEXT));
	}

	public TopMenuController getTopMenuController() {
	  return topMenuController;
  }

	public ApplicationFrame getApplicationFrame(){
		return this.applicationFrame;
	}
	
}
