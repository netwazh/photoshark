package com.home.photoshark.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import com.home.photoshark.model.ApplicationModel;
import com.home.photoshark.model.ResourceBundleModel;

/**
 * The frame(window) of the application.
 * 
 */
public class ApplicationFrame extends JFrame {

	private Logger logger = Logger.getLogger(ApplicationFrame.class.getName());
	private static final long serialVersionUID = 1L;

	private ToolsPanelView toolsPanel;
	private InfoPanelView infoPanel;
	private MainPanelView mainPanel;
	private TopMenuView topMenu;

	// application-level model declared
	// TODO: Maybe merge resourceBundleModel in to application model? Because it
	// is also some kind of "Application-wide" model.
	public ResourceBundleModel resourceBundleModel;
	public ApplicationModel applicationModel;

	public ApplicationFrame(String title, int width, int height) {
		logger.addHandler(Application.logFileHandler);
		logger.info("initialize application frame");

		this.setTitle(title);
		this.setSize(width, height);
		this.setIconImage(new ImageIcon("resource/image/shark.png").getImage());
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(screenSize.width / 2 - width / 2, screenSize.height / 2 - height / 2);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// add application-wide model
		this.resourceBundleModel = new ResourceBundleModel();
		this.applicationModel = new ApplicationModel();

		// add top-level views to application frame
		toolsPanel = new ToolsPanelView(this);
		this.getContentPane().add(toolsPanel, BorderLayout.NORTH);
		infoPanel = new InfoPanelView(this);
		this.getContentPane().add(infoPanel, BorderLayout.SOUTH);
		mainPanel = new MainPanelView(this);
		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
		topMenu = new TopMenuView(this);
		this.setJMenuBar(topMenu);

		// add observers for resourceBundleModel
		// these controllers listen to global resourceBundleModel change, and do
		// changes on their respective model and view
		resourceBundleModel.addObserver(topMenu.getTopMenuController());
		resourceBundleModel.addObserver(toolsPanel.getToolsPanelController());
		resourceBundleModel.addObserver(infoPanel.getInfoPanelController());

		// add observer controllers for applicationModel
		applicationModel.addObserver(mainPanel.getDetailsPanel().getDetailsPanelController());
		applicationModel.addObserver(mainPanel.getFolderPanel().getFolderPanelController());
		applicationModel.addObserver(infoPanel.getInfoPanelController());

	}

	public MainPanelView getMainPanel() {
		return this.mainPanel;
	}

}
